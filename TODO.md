== To-Dos

* Unit testing
* Add columns to display switch
* Add no color switch
* Add target switching based on known targets
* When searching with filters add the filter columns to the result
* Add resource methods to the api: api[resource].get | put
* no paging on non-tty
