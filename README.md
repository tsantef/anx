== Installation

    npm install appnexus-cli -g

==== Dev Installation

    git clone
    cd
    npm link

== Login

    anx target [target]
    anx login

=== Targets

    Prod: api.appnexus.com
    Dev: hb.sand-08.adnxs.net

== Usage

    Usage: anx <command> [options]

    Commands:

      get [url]              make a GET request to target
      post [url] [payload]   make a POST request to target
      advertiser [advertiser_id] get a single Advertiser record by Id
      advertisers            get a list Advertiser records
      creative [creative_id] get a single Creative record by Id
      creatives              get a list Creative records
      member [member_id]     get a single Member record by Id
      members                get a list Member records
      publisher [publisher_id] get a single Publisher record by Id
      publishers             get a list Publisher records
      template [template_id] get a single Template record by Id
      templates              get a list Template records
      user [user_id]         get a single User record by Id
      users                  get a list User records
      login [username]       run remote setup commands
      logout                 logout by removing session token
      switch-user [user_id]  switch session user
      target [url]           set target or view current target
      targets                list known targets

    Options:

      -h, --help                 output usage information
      -V, --version              output the version number
      -s, --pagesize [pagesize]  page size (default 25)
      -t, --trace                print all http requests
      -j, --json                 print as json
      -f, --file <file>          post a file
      -m, --meta                 get meta data
