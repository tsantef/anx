var _ = require('lodash');

module.exports = function(program) {

	program
		.command('get [url]')
		.description("make a GET request to target")
		.action(function(url) {
			program.apiWrapper(_.partial(program.api.getJson, url), function (err, res, body) {
				if (err) { return program.handleError(err); }
				console.log('GET:'.cyan);
				console.log(program.prettyJson(body));
			});
		});

	program
		.command('post [url] [payload]')
		.description("make a POST request to target")
		.action(function(url, payload) {
			program.apiWrapper(_.partial(program.api.post, url, payload), function (err, res, body) {
				if (err) { return program.handleError(err); }
				console.log('POST:');
				console.log(program.prettyJson(body));
			});
		});

};
