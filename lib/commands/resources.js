var _ = require('lodash');
var Table = require('../table');
var inflection = require('inflection');

module.exports = function (program) {

	function metaTable(fields) {
		var table = new Table({
			head: ['Name', 'Type', 'Sort By', 'Filter By']
		});
		fields.forEach(function (field) {
			table.push([
				program.theme.getColor('columns', 'name', field.name), (field.type === 'array of objects') ? metaTable(field.fields).toString() : program.theme.getColor('types', field.type, field.type),
				program.theme.getColor('values', field.sort_by.toString(), field.sort_by.toString()), // jshint ignore:line
				program.theme.getColor('values', field.filter_by.toString(), field.filter_by.toString()) // jshint ignore:line
			]);
		});
		return table;
	}

	function renderMetaData(resourceName) {
		program.apiWrapper(_.partial(program.api.getJson, '/' + resourceName + '/meta'), function (err, res, body) {
			if (err) {
				return program.handleError(err);
			}
			if (program.json) {
				console.log(program.prettyJson(body));
			}
			else {
				console.log(metaTable(body.response.fields).toString());
			}
		});
	}

	function detailsTable(data) {
		var table = new Table({
			head: ['Name', 'Value']
		});
		_.keys(data).forEach(function (key) {
			var row = {};
			var value = data[key];
			if (_.isObject(value)) {
				row[key] = program.prettyJson(value);
			}
			else {
				var type = _.typeOf(value);
				row[key] = program.theme.getColor('types', type, value);
			}

			table.push(row);
		});
		return table;
	}

	function renderDetails(data) {
		if (program.json) {
			console.log(program.prettyJson(data));
		}
		else {
			console.log(detailsTable(data).toString());
		}
	}

	_.keys(program.apiResources).forEach(function (resourceKey) {
		var resource = program.apiResources[resourceKey];

		_.defaults(resource, {
			singular: inflection.singularize(resourceKey),
			listColumns: {
				'id': {},
				'name': {}
			}
		});

		program
			.command(resource.singular + ' [' + resource.singular + '_id]')
			.description('get a single ' + _.capitalize(resource.singular) + ' record by Id')
			.action(function (id) {
				id = parseInt(id);
				if (_.isNaN(id)) {
					return program.errorMessage('Invalid Id');
				}
				if (program.meta) {
					return renderMetaData(resource.singular);
				}
				program.apiWrapper(_.partial(program.api.getJson, '/' + resource.singular + '?id=' + id), function (err, res, body) {
					if (err) {
						return program.handleError(err);
					}
					if (program.api.hasRecord(body)) {
						renderDetails(body.response[resource.singular]);
					}
					else {
						console.log('No %s record found with id %s', resource.singular.yellow, id.toString().yellow);
					}
				});
			});

		program
			.command(resourceKey)
			.description('get a list ' + _.capitalize(resource.singular) + ' records')
			.action(function () {
				var searchRaw = Array.prototype.slice.call(arguments, 0, arguments.length - 1);
				var searchParams = searchRaw.join('&');
				if (program.meta) {
					return renderMetaData(resource.singular);
				}
				if (program.json) {
					program.apiWrapper(_.partial(program.api.getJson, '/' + resource.singular + '?' + searchParams), function (err, res, body) {
						if (err) {
							return program.handleError(err);
						}
						console.log(program.prettyJson(body.response[resourceKey]));
					});
				}
				else {
					program.wrapWithPaging({
						url: '/' + resource.singular + '?' + searchParams
					}, function (err, res, body, next) {
						if (err) {
							return program.handleError(err);
						}

						if (program.api.hasRecord(body)) {
							var header = [];
							_.keys(resource.listColumns).forEach(function (column) {
								header.push(resource.listColumns[column].label || inflection.titleize(column));
							});
							var table = new Table({
								head: header
							});
							var listRecords = function (record) {
								var row = [];
								_.keys(resource.listColumns).forEach(function (column) {
									if (record[column]) {
										row.push(program.theme.getColor('columns', column, record[column].toString()));
									}
									else {
										row.push(program.theme.getColor('types', 'null', 'null'));
									}
								});
								table.push(row);
							};
							if (body.response[resourceKey]) {
								body.response[resourceKey].forEach(listRecords);
							}
							if (body.response[resource.singular]) {
								listRecords(body.response[resource.singular]);
							}
							console.log(table.toString());
						}
						else {
							console.log('No %s records found', resource.singular.yellow);
						}
						next();
					});
				}
			});
	});

};
