module.exports = function(program) {

	function login (username, callback) {
		var prompts = [
			{ name: 'username', message: 'Username:', default: username || program.session.username },
			{ name: 'password', message: 'Password:', hidden: true}
		];

		function attemptLogin(max, attempts) {
			attempts = attempts || 1;
			program.prompt.get(prompts, function (err, result) {
				if (err) { return callback(err); }
				program.spinner.start();
				program.api.login(result.username, result.password, function (err, token) {
					program.spinner.stop();
					if (token) {
						program.session.token = token;
						program.session.username = result.username;
						program.session.save();
						return callback(null, token);
					} else {
						if (attempts < max) {
							if (err && err.message) { program.errorMessage(err.message); }
							return attemptLogin(max, attempts + 1);
						} else {
							return callback(err);
						}
					}
				});
			});
		}

		attemptLogin(3);
	}

	function switchToUser(userId, callback) {
		program.spinner.start();
		program.api.switchToUser(userId, function (err, token) {
			program.spinner.stop();
			return callback(err, token);
		});
	}

	program
		.command('login [username]')
		.description('run remote setup commands')
		.action(function(username) {
			login(username, function (err, token) {
				if (token) {
					program.successMessage('Login successful');
				} else {
					program.errorMessage('Login failed');
				}
			});
	  });

	program
		.command('logout')
		.description("logout by removing session token")
		.action(function() {
			program.api.token = '';
			program.session.token = '';
			program.session.save();
			program.successMessage('Logout successful');
	  });

	program
		.command('switch-user [user_id]')
		.description("switch session user")
		.action(function(userId) {
			switchToUser(userId, function (err, token) {
				if (token) {
					program.successMessage('Switch user successful');
				} else {
					program.errorMessage('Switch user failed');
				}
			});
	  });

	return { login: login };

};
