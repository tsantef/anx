var fs = require('fs');

module.exports = function(program) {
	var commands = {};

	fs.readdirSync(__dirname).forEach(function (filename) {
		var name = filename.substr(0, filename.lastIndexOf('.')).toLowerCase();
		if (name !== 'index') {
			commands[name] = require('./' + filename)(program);
		}
	});

	return commands;
};
