var url = require('url');

module.exports = function(program) {

	program
		.command('target [url]')
		.description("set target or view current target")
		.action(function(target) {
			if (target) {
				if (!target.match(/(http:\/\/|https:\/\/)/)) {
					target = 'https://' + target;
				}
				var parsedTarget = url.parse(target);
				program.session.target = url.format(parsedTarget);
				program.session.save();
				program.api.target = program.session.target;
				program.api.token = program.session.token;
				console.log('Target set to: ' + program.session.target.yellow);
			} else {
				console.log(program.session.target);
			}
		});

	program
		.command('targets')
		.description("list known targets")
		.action(function() {
			program.session.targets.forEach(function (target) {
				console.log(target);
			});
		});

};
