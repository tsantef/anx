var _ = require('lodash');
var fs = require('fs');

module.exports = function(config) {

	var configs;

	try {
		configs = JSON.parse(fs.readFileSync(config.path, 'utf8') || '{}');
	} catch(err) {
		configs = {};
	}

	_.defaults(configs, config.defaults);

	return configs;

}
