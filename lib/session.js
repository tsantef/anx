var _ = require('lodash');
var fs = require('fs');

module.exports = function(config) {
	var session = {};

	function loadSession() {
		try {
			session = JSON.parse(fs.readFileSync(config.path, 'utf8') || '{}');
		} catch(err) {
			session = {};
		}
		session.tokens = session.tokens || {};
	}

	loadSession();

	return {
		get username() {
			return session.username;
		},
		set username(val) {
			session.username = val;
		},

		get target() {
			return session.target;
		},
		set target(val) {
			session.target = val;
		},

		get targets() {
			return _.keys(session.tokens);
		},

		get token() {
			return session.tokens[session.target];
		},
		set token(val) {
			session.tokens[session.target] = val;
		},

		save: function() {
			fs.writeFileSync(config.path, JSON.stringify(session, null, 2));
		}
	};

};
