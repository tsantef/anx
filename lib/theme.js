var _ = require('lodash');

module.exports = function (config) {
	return {
		getColor: function (group, type, value) {
			if (_.typeOf(value) === 'boolean') {
				value = value ? 'true' : 'false';
			}
			value = (value || 'null').toString();
			if (_.has(config[group], type)) {
				return value[config[group][type]];
			}
			else {
				return value;
			}
		}
	};
};
