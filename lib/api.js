var _ = require('lodash');

var Api = function (config) {
	var _self = this;
	_self.target = config.target || '';
	_self.token = config.token || '';
	_self.request = config.request || require('request');

	function _request(req, callback) {
		req = req || {};
		req.headers = {};
		if (!_self.target) {
			return callback(new Error('Target not set'));
		}
		if (_self.token) {
			req.headers.Authorization = _self.token;
		}
		var baseUrl = _self.target.replace(/\/$/, '');
		req.uri = baseUrl + '/' + req.path.replace(/^\//, '');
		_self.request(req, function (err, res, body) {
			return callback(err, res, body);
		});
	}

	function _get(req, callback) {
		req.method = 'GET';
		_request(req, callback);
	}

	function _post(req, callback) {
		req.method = 'POST';
		_request(req, callback);
	}

	_self.get = function (path, callback) {
		_get({
			path: path
		}, callback);
	};

	_self.getJson = function (path, callback) {
		_get({
			path: path,
			json: true
		}, callback);
	};

	_self.post = function (path, body, callback) {
		_post({
			path: path,
			body: body
		}, callback);
	};

	_self.postJson = function (path, body, callback) {
		_post({
			path: path,
			body: _.isObject(body) ? JSON.stringify(body) : body,
			json: true
		}, callback);
	};

	_self.login = function (username, password, callback) {
		_self.token = null;
		_self.postJson('/auth', {
			auth: {
				username: username,
				password: password
			}
		}, function (err, res, body) {
			if (err) {
				return callback(err);
			}
			if (res.statusCode === 200 && Api.prototype.statusOk(body)) {
				_self.token = body.response.token;
				callback(null, _self.token);
			}
			else {
				callback(new Error(body.response.error));
			}
		});
	};

	_self.switchToUser = function (userId, callback) {
		_self.postJson('/auth', {
			auth: {
				switch_to_user: userId // jshint ignore:line
			}
		}, function (err, res, body) {
			if (err) {
				return callback(err);
			}
			if (res.statusCode === 200 && Api.prototype.statusOk(body)) {
				return callback(null);
			}
			else {
				return callback(new Error(body.response.error));
			}
		});
	};

	return _self;
};

Api.prototype.statusOk = function statusOk(body) {
	return body && body.response && body.response.status === 'OK';
};

Api.prototype.hasRecord = function (body) {
	return this.statusOk(body) && body.response.count > 0;
};

module.exports = Api;
