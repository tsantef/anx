var _ = require('lodash');

module.exports = function (options) {
	options = options || {};
	_.defaults(options, {
		// sprites: ['   ', '.  '.cyan, '.. '.cyan, '...'.cyan, ' ..'.cyan, '  .'.cyan],
		sprites: ['['.cyan + '='.cyan + '  ]'.grey, '[ '.grey + '='.cyan + ' ]'.grey, '[  '.grey + '='.cyan + ']'.cyan, '[ '.grey + '='.cyan + ' ]'.grey],
		interval: 175
	});
	var spriteIndex = 0;
	var intervalId;

	function hideCursor() {
		process.stdout.write('\033[?25l');
	}

	function showCursor() {
		process.stdout.write('\033[?25h');
	}

	function drawSpinner() {
		process.stdout.write(options.sprites[spriteIndex]);
	}

	function clearSpinner() {
		process.stdout.write('\033[' + options.sprites[spriteIndex].length + 'D');
		process.stdout.write(new Array(options.sprites[spriteIndex].length + 1).join(' '));
		process.stdout.write('\033[' + options.sprites[spriteIndex].length + 'D');
	}

	process.on('exit', function () {
		showCursor();
	});

	return {
		start: function () {
			if (process.stdout.isTTY) {
				hideCursor();
				drawSpinner();
				intervalId = setInterval(function () {
					clearSpinner();
					spriteIndex = ++spriteIndex % options.sprites.length;
					drawSpinner();
				}, options.interval);
			}
		},
		stop: function () {
			if (process.stdout.isTTY) {
				showCursor();
				clearInterval(intervalId);
				clearSpinner();
				spriteIndex = 0;
			}
		}
	};
};
