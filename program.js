var _ = require('lodash');
var Api = require('./lib/api');
var theme = require('./lib/theme');
var config = require('./lib/config');
var mixins = require('./lib/mixins');
var spinner = require('./lib/spinner');
var session = require('./lib/session');
var request = require('request');
var program = require('commander');
var commands = require('./lib/commands');
var configJson = require('./config.json');
var packageJson = require('./package.json');

var appConfig = config({
	path: process.env.HOME + '/.anxrc',
	defaults: configJson
});

program.prompt = require('prompt');
program.prompt.message = '';
program.prompt.delimiter = '';
program.prompt.colors = false;

program.spinner = spinner();
program.theme = theme(appConfig.theme);

program.apiResources = appConfig.resources;

program
	.version(packageJson.version)
	.usage('<command> [options]')
	.option('-s, --pagesize [pagesize]', 'page size (default 25)')
	.option('-t, --trace', 'print all http requests')
	.option('-j, --json', 'print as json')
	.option('-f, --file <file>', 'post a file')
	.option('-m, --meta', 'get meta data');

program.cmds = commands(program);

// Preparse options
program.parseOptions(process.argv);

// Load Session
program.session = session({
	path: process.env.HOME + '/.anx-session'
});

program.prettyJson = function (obj) {
	if (obj === undefined) {
		return 'undefined'.yellow;
	}
	var output;
	output = JSON.stringify(obj, null, 2);
	output = output.replace(/(\".*?\":)/g, '$1'.grey);
	output = output.replace(/( true|false)(,|\n)/g, program.theme.getColor('types', 'boolean', '$1') + '$2');
	output = output.replace(/( \d*\.?\d*)(,|\n)/g, program.theme.getColor('types', 'number', '$1') + '$2');
	output = output.replace(/( null)(,|\n)/g, program.theme.getColor('types', 'null', '$1') + '$2');
	return output;
};

// Request wrapper
function traceRequest(opts, callback) {
	if (program.trace) {
		console.log('REQUEST: '.cyan + program.prettyJson(opts));
	}
	return request(opts, function (err, res, body) {
		if (err) {
			if (program.trace) {
				program.errorMessage('ERROR: ' + err.message);
			}
			return callback(err, res, body);
		}
		else {
			if (program.trace) {
				console.log('RESPONSE: '.cyan + program.prettyJson(res.headers));
				console.log('BODY: '.cyan + program.prettyJson(res.body));
				console.log('');
			}
			return callback(err, res, body);
		}
	});
}

program.api = new Api({
	request: traceRequest,
	target: program.session.target,
	token: program.session.token
});

program.apiWrapper = function (method, callback) {
	program.spinner.start();
	method(function (err, res, body) {
		program.spinner.stop();
		if (err) {
			return callback(err, res, body);
		}
		if (res.statusCode === 401) {
			program.cmds.session.login('', function (err, token) {
				if (err) {
					return program.errorMessage(err.message);
				}
				if (token) {
					program.apiWrapper(method, callback);
				}
			});
		}
		else {
			return callback(err, res, body);
		}
	});
};

program.wrapWithPaging = function (opts, callback) {
	_.defaults(opts, {
		startElement: 0,
		numElements: program.pagesize || 25,
		delimiter: (!opts.url.match(/\?/)) ? '?' : '&'
	});

	var pagedUrl = _.template('${url}${delimiter}start_element=${startElement}&num_elements=${numElements}', opts);

	program.apiWrapper(_.partial(program.api.getJson, pagedUrl), function (err, res, body) {
		callback(err, res, body, function () {
			if (program.api.hasRecord(body)) {
				var message = _.template('[Records ${from} to ${to} of ${total}]', {
					from: (opts.startElement + 1).toString().cyan,
					to: (Math.min(opts.startElement + opts.numElements, opts.startElement + body.response.count)).toString().cyan,
					total: body.response.count.toString().cyan
				});

				opts.startElement = opts.startElement + opts.numElements;

				if (opts.startElement < body.response.count) {
					program.prompt.get({
						name: 'yesno',
						message: message + ' View More? (Y/n)'
					}, function (err, result) {
						if (err) {
							return err;
						}
						result.yesno = result.yesno || 'Y';
						if (result.yesno.match(/Y|y/)) {
							program.wrapWithPaging(opts, callback);
						}
					});
				}
				else {
					console.log(message);
				}
			}
		});
	});
};

program.successMessage = function (msg) {
	console.log(msg.green)
}

program.errorMessage = function (msg) {
	console.log(msg.red)
}

program.handleError = function (err) {
	// if (err === 'getaddrinfo') {
	// 	return program.errorMessage('Connection interrupted');
	// }
	if (err.message) {
		program.errorMessage('Error: ' + err.message);
	}
};

var colors = require('colors');
colors.mode = process.stdout.isTTY ? colors.mode : 'none';

program.parse(process.argv);

if (!_.isObject(program.args.slice(-1)[0])) {
	program.help();
}
