var Api = require('./lib/api');

module.exports = function(options) {
	var _self = this;

	_self.api = new Api({
		request: options.request || require('request'),
		target: options.target || '',
		token: options.token || ''
	});

	return _self.api;
};
